<?php
/*
  ./src/Entity/Page.php
 */
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PageRepository")
 * Entité de Page
 */
class Page
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * id de Film
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     * titre de Page
     * @var int
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=45)
     * slug de Page
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     * texte de page
     * @var string|null
     */
    private $texte;

    /**
     * @ORM\Column(type="integer")
     * numéro d'ordre de Page
     * @var int
     */
    private $tri;
    /**
     * recupère l'id
     * @return int renvois l'id
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * recupère le titre
     * @return string renvois le titre
     */
    public function getTitre(): ?string
    {
        return $this->titre;
    }
    /**
     * définit le titre
     * @param  string $titre titre à insérer
     * @return self          renvois la Page
     */
    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }
    /**
     * recupère le slug
     * @return string rnvoies le slug
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }
    /**
     * definit le slug
     * @param  string $slug slug à ajouter
     * @return self         renvois Page
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
    /**
     * récupère le texte
     * @return string|null renvois le texte
     */
    public function getTexte(): ?string
    {
        return $this->texte;
    }
    /**
     * définit le texte
     * @param  string $texte texte à ajouter
     * @return self           renvois Page
     */
    public function setTexte(?string $texte): self
    {
        $this->texte = $texte;

        return $this;
    }
    /**
     * récupère le numéro d'ordre
     * @return int renvois le numéro d'ordre
     */
    public function getTri(): ?int
    {
        return $this->tri;
    }
    /**
     * définit le numéro d'ordre
     * @param  int  $tri numéro d'ordre à insérer
     * @return self      renvois Page
     */
    public function setTri(int $tri): self
    {
        $this->tri = $tri;

        return $this;
    }
}
