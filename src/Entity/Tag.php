<?php
/*
  ./src/Entity/Tag.php
 */
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 * Entité de Tag
 */
class Tag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * id de Tag
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     * nom de tag
     * @var string
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=45)
     * slug de Tag
     * @var string
     */
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Creature", mappedBy="tags")
     * Creatures de Tag
     * @var creature[]|Collection
     */
    private $creatures;
    /**
     * constructeur automatique
     */
    public function __construct()
    {
        $this->creatures = new ArrayCollection();
    }
    /**
     * recupère l'id
     * @return int renvoies l'id
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * recupère le nom
     * @return string renvois le nom
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }
    /**
     * definit le nom
     * @param  string $nom nom à insérer
     * @return self        renvois le Tag
     */
    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
    /**
     * recupère le slug
     * @return string renvois le slug
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }
    /**
     * Définit le slug
     * @param  string $slug slug à insérer
     * @return self         renvois le Tag
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * récupère les Creatures
     * @return Collection|Creature[] renvois les objets Creature de Tag
     */
    public function getCreatures(): Collection
    {
        return $this->creatures;
    }
    /**
     * ajoute une Creature à Tag
     * @param  Creature $creature Objet Creature à ajouter
     * @return self               renvois le Tag
     */
    public function addCreature(Creature $creature): self
    {
        if (!$this->creatures->contains($creature)) {
            $this->creatures[] = $creature;
            $creature->addTag($this);
        }

        return $this;
    }
    /**
     * retire une Creature à Tag
     * @param  Creature $creature objet Creature à supprimer
     * @return self               renvois le Tag
     */
    public function removeCreature(Creature $creature): self
    {
        if ($this->creatures->contains($creature)) {
            $this->creatures->removeElement($creature);
            $creature->removeTag($this);
        }

        return $this;
    }
}
