<?php
/*
  ./src/Entity/Film.php
 */
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FilmRepository")
 * Entité de Film
 */
class Film
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * id de Film
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     * titre de Film
     * @var string
     */
    private $titre;

    /**
     * @ORM\Column(type="text", nullable=true)
     * Synopsis de Film
     * @var string|null
     */
    private $synopsis;

    /**
     * @ORM\Column(type="string", length=45)
     * slug de Film
     * @var string
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Creature", mappedBy="film")
     * Creatures de Film
     * @var Creature[]|Collection
     */
    private $creatures;
    /**
     * constructeur automatique
     */
    public function __construct()
    {
        $this->creatures = new ArrayCollection();
    }
    /**
     * recupère l'id
     * @return int renvoies l'id
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * recupère le titre de Film
     * @return string renvoies le titre de Film
     */
    public function getTitre(): ?string
    {
        return $this->titre;
    }
    /**
     * définit le titre de Film
     * @param  string $titre titre à insérer
     * @return self          renvoies le Film
     */
    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }
    /**
     * recupère le synopsis
     * @return string|null renvoies le synopsis
     */
    public function getSynopsis(): ?string
    {
        return $this->synopsis;
    }
    /**
     * définit le synopsis
     * @param  string $synopsis synopsis à insérer
     * @return self              renvois le Film
     */
    public function setSynopsis(?string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }
    /**
     * récupère le slug
     * @return string renvoies le slug
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }
    /**
     * définit le slug
     * @param  string $slug slug à insérer
     * @return self         renvoies le Film
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * recupère les créatures de Film
     * @return Collection|Creature[] renvoies les objets Creatures de Film
     */
    public function getCreatures(): Collection
    {
        return $this->creatures;
    }
    /**
     * Ajoute une Creature à Film
     * @param  Creature $creature Objet Creature à ajouter
     * @return self               renvois le Film
     */
    public function addCreature(Creature $creature): self
    {
        if (!$this->creatures->contains($creature)) {
            $this->creatures[] = $creature;
            $creature->setFilm($this);
        }

        return $this;
    }
    /**
     * Retire une Creature à Film
     * @param  Creature $creature objet Creature à retirer
     * @return self               renvois le Film
     */
    public function removeCreature(Creature $creature): self
    {
        if ($this->creatures->contains($creature)) {
            $this->creatures->removeElement($creature);
            // set the owning side to null (unless already changed)
            if ($creature->getFilm() === $this) {
                $creature->setFilm(null);
            }
        }

        return $this;
    }
}
