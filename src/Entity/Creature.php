<?php
/*
  ./src/Entity/Creature.php
 */
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CreatureRepository")
 * Entité de Creature
 */
class Creature
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * id de Creature
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     * nom de Creature
     * @var string
     */
    private $nom;

    /**
     * @ORM\Column(type="text", nullable=true)
     * texte principal de Creature
     * @var string|null
     */
    private $texteLead;

    /**
     * @ORM\Column(type="text", nullable=true)
     * texte secondaire de Creature
     * @var string|null
     */
    private $texteSuite;

    /**
     * @ORM\Column(type="datetime")
     * date de création de Creature
     * @var \DateTimeInterface
     */
    private $dateCreation;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     * image de Creature
     * @var string|null
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=45)
     * nom slugifié de Creature
     * @var string
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Film", inversedBy="creatures")
     * film de Creature
     * @var \App\Entity\Film|null
     */
    private $film;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="creatures")
     * tags de Creature
     * @var (\App\Entity\Tag)[]|Collection
     */
    private $tags;
    /**
     * contruction automatique
     */
    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->setDateCreation(new \DateTime());
    }
    /**
     * affiche l'id de Creature
     * @return int renvoies l'id de Creature
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * affiche nom de Creature
     * @return string renvoies le nom de Creature
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }
    /**
     * definit nom de Creature
     * @param  string $nom nom à donner
     * @return self        renvoies l'objet en cours
     */
    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
    /**
     * affiche le texte principal
     * @return string|null renvoies le texte principal
     */
    public function getTexteLead(): ?string
    {
        return $this->texteLead;
    }
    /**
     * definit texte principal de Creature
     * @param  string $nom texte à définir
     * @return self        renvoies l'objet en cours
     */
    public function setTexteLead(?string $texteLead): self
    {
        $this->texteLead = $texteLead;

        return $this;
    }
    /**
     * affiche le texte secondaire
     * @return string|null renvoies le texte secondaire
     */
    public function getTexteSuite(): ?string
    {
        return $this->texteSuite;
    }
    /**
     * definit texte secondaire de Creature
     * @param  string $nom texte à définir
     * @return self        renvoies l'objet en cours
     */
    public function setTexteSuite(?string $texteSuite): self
    {
        $this->texteSuite = $texteSuite;

        return $this;
    }
    /**
     * affiche la date de creation
     * @return \DateTimeInterface renvoies la date de creation
     */
    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }
    /**
     * Definit la date de creation
     * @param  \DateTimeInterface $dateCreation date à inserer
     * @return self                            retourne l'objet en cours
     */
    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }
    /**
     * affiche le lien de l'image
     * @return string|null renvoies l'image
     */
    public function getImage(): ?string
    {
        return $this->image;
    }
    /**
     * definit le lien de l'image
     * @param  string $image image a inserer
     * @return self           renvoies l'objet en cours
     */
    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }
    /**
     * recupère le slug
     * @return string renvoies le slug
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }
    /**
     * définit le slug
     * @param  string $slug slug à insérer
     * @return self         renvoies l'objet en cours
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
    /**
     * recupère l'objet Film
     * @return Film|null renvoies le Film
     */
    public function getFilm(): ?Film
    {
        return $this->film;
    }
    /**
     * definit le Film à insérer
     * @param  Film $film objet Film à insérer
     * @return self        renvois l'objet en cours
     */
    public function setFilm(?Film $film): self
    {
        $this->film = $film;

        return $this;
    }

    /**
     * recupère les les objets Tags de Creature
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }
    /**
     * ajoute un objet Tag à Creature
     * @param  Tag  $tag Objet à ajoute
     * @return self      renvoies Creature
     */
    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }
    /**
     * Retire un objet Tag de Creature
     * @param  Tag  $tag objet Tag à supprimer
     * @return self      renvoie Creature
     */
    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    /**
     * Définit un slug à partir d'un nom
     * @param  string $string    string à slugifier
     * @param  string $delimiter caractère délimitant chaque mot
     * @return string  string slugifié
     */
    function slugify($string, $delimiter = '-') {
    	$oldLocale = setlocale(LC_ALL, '0');
    	setlocale(LC_ALL, 'en_US.UTF-8');
    	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
    	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    	$clean = strtolower($clean);
    	$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
    	$clean = trim($clean, $delimiter);
    	setlocale(LC_ALL, $oldLocale);
    	return $clean;
    }
}
