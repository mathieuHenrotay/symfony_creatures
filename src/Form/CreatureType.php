<?php
// ./src/Form/CreatureType.php
namespace App\Form;

use App\Entity\Creature;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
/**
 * Formulaire type basé sur l'entité Creature
 */
class CreatureType extends AbstractType
{
    /**
     * permet la construction d'un formulaire basé sur l'entité Creature
     * @param  FormBuilderInterface $builder chargé de construire le form
     * @param  array                $options options de construction
     * @return void                        ne renvoit rien
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('texteLead')
            ->add('texteSuite')
            ->add('image')
            ->add('film', EntityType::class, [
              'class' => \App\Entity\Film::class,
              'choice_value' => 'id',
              'choice_label' => 'titre'
            ])
            ->add('tags', EntityType::class, [
                    'class' => \App\Entity\Tag::class,
                    'choice_value' => 'id',
                    'choice_label' => 'nom',
              		'multiple' => true,
              		'expanded' => true
              ])
        ;
    }
    /**
     * configure les options
     * @param  OptionsResolver $resolver possède les fonctions pour définir les options
     * @return void                    ne renvoit rien
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Creature::class,
        ]);
    }
}
