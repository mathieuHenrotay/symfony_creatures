<?php
// App/Extensions/Doctrine/MatchAgainst.php

namespace App\Extensions\Doctrine;

use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;

/**
 * "MATCH_AGAINST" "(" {StateFieldPathExpression ","}* InParameter {Literal}? ")"
 * Permet l'utilisation de la fonction MATCH_AGAINST() dans doctrine
 */
class MatchAgainst extends FunctionNode {
    /**
     * colonne
     * @var array
     */
    public $columns = array();
    public $needle;
    public $mode;
    /**
     * parse le SQL
     * @param  DoctrineORMQueryParser $parser permet de parcourir
     * @return void                         ne renvois rien
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser) {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        do {
            $this->columns[] = $parser->StateFieldPathExpression();
            $parser->match(Lexer::T_COMMA);
        } while ($parser->getLexer()->isNextToken(Lexer::T_IDENTIFIER));
        $this->needle = $parser->InParameter();
        while ($parser->getLexer()->isNextToken(Lexer::T_STRING)) {
            $this->mode = $parser->Literal();
        }
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
    /**
     * renvoies le sql
     * @param  DoctrineORMQuerySqlWalker $sqlWalker [description]
     * @return string                               renvoies un morceau de requête
     */
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker) {
        $haystack = null;
        $first = true;
        foreach ($this->columns as $column) {
            $first ? $first = false : $haystack .= ', ';
            $haystack .= $column->dispatch($sqlWalker);
        }
        $query = "MATCH(" . $haystack .
                ") AGAINST (" . $this->needle->dispatch($sqlWalker);
        if ($this->mode) {
            $query .= " " . str_replace("'","",$this->mode->dispatch($sqlWalker)) . " )";
        } else {
            $query .= " )";
        }

        return $query;
    }

}
