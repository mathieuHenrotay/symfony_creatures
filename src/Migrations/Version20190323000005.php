<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190323000005 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE creature (id INT AUTO_INCREMENT NOT NULL, film_id INT DEFAULT NULL, nom VARCHAR(45) NOT NULL, texte_lead LONGTEXT DEFAULT NULL, texte_suite LONGTEXT DEFAULT NULL, date_creation DATETIME NOT NULL, image VARCHAR(45) DEFAULT NULL, slug VARCHAR(45) NOT NULL, INDEX IDX_2A6C6AF4567F5183 (film_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE creature_tag (creature_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_10558C02F9AB048 (creature_id), INDEX IDX_10558C02BAD26311 (tag_id), PRIMARY KEY(creature_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE film (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(45) NOT NULL, synopsis LONGTEXT DEFAULT NULL, slug VARCHAR(45) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE page (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(45) NOT NULL, slug VARCHAR(45) NOT NULL, texte LONGTEXT DEFAULT NULL, tri INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(45) NOT NULL, slug VARCHAR(45) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE creature ADD CONSTRAINT FK_2A6C6AF4567F5183 FOREIGN KEY (film_id) REFERENCES film (id)');
        $this->addSql('ALTER TABLE creature_tag ADD CONSTRAINT FK_10558C02F9AB048 FOREIGN KEY (creature_id) REFERENCES creature (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE creature_tag ADD CONSTRAINT FK_10558C02BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE creature_tag DROP FOREIGN KEY FK_10558C02F9AB048');
        $this->addSql('ALTER TABLE creature DROP FOREIGN KEY FK_2A6C6AF4567F5183');
        $this->addSql('ALTER TABLE creature_tag DROP FOREIGN KEY FK_10558C02BAD26311');
        $this->addSql('DROP TABLE creature');
        $this->addSql('DROP TABLE creature_tag');
        $this->addSql('DROP TABLE film');
        $this->addSql('DROP TABLE page');
        $this->addSql('DROP TABLE tag');
    }
}
