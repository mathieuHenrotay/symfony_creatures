<?php
/*
  ./src/Controller/TagController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Tag;
use Symfony\Component\HttpFoundation\Request;
/**
 * controleur des Page
 */
class TagController extends GenericController {
    /**
     * renvoies une vue avec l'index des pages
     * @param  string $vue     vue à renvoyer
     * @param  array  $orderBy ordination eventuelle ['champ'=>'sens']
     * @param  int $limit   limitation éventuelle
     * @return Symfony\Component\HttpFoundation\Response          vue avec l'index des Tag dedans
     */
    public function indexAction(string $vue = 'index', array $orderBy = ['nom' => 'ASC'], int $limit = null){
      $tags = $this->_repository->findBy([], $orderBy, $limit);
      return $this->render('tags/'.$vue.'.html.twig',[
        'tags' => $tags
      ]);
    }

}
