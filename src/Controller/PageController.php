<?php
/*
  ./src/Controller/PageController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Page;
use Symfony\Component\HttpFoundation\Request;
/**
 * Controleur des Page
 */
class PageController extends GenericController {
    /**
     * renvoies une vue avec la liste des Page
     * @param  array $activeParams ensemble des paramètres de la route actuelle
     * @param  string $activeRoute  nom de la route actuelle
     * @param  string $vue          vue à renvoyer
     * @param  array  $orderBy      ordination eventuelle ['champ'=>'sens']
     * @param  int $limit        limitation éventuelle
     * @return Symfony\Component\HttpFoundation\Response               vue avec l'ensemble des Page disponibles
     */
    public function indexAction(array $activeParams = null, string $activeRoute, string $vue = 'index', array $orderBy = ['tri' => 'ASC'], int $limit = null){
      $pages = $this->_repository->findBy([], $orderBy, $limit);

      if( $activeRoute == 'app_pages_show' || $activeRoute == 'app_homepage' ):
        $activeId = $activeParams['id'];
      else:
        $activeId=null;
      endif;
      return $this->render('pages/'.$vue.'.html.twig',[
        'pages' => $pages,
        'activeId' => $activeId
      ]);
    }

}
