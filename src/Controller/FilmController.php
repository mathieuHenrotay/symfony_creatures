<?php
/*
  ./src/Controller/FilmController.php
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Film;
use Symfony\Component\HttpFoundation\Request;
/**
 * Le controleurs des Film
 */
class FilmController extends GenericController {
    /**
     * affiche la liste des Film
     * @param  string $vue     vue à afficher
     * @param  array  $orderBy ordination eventuelle ['champ'=>'sens']
     * @param  int $limit   limitation du nombre de résultat
     * @return Symfony\Component\HttpFoundation\Response          revoie une vue avec les films disponibles dedans
     */
    public function indexAction(string $vue = 'index', array $orderBy = ['titre' => 'ASC'], int $limit = null){
      $films = $this->_repository->findBy([], $orderBy, $limit);
      return $this->render('films/'.$vue.'.html.twig',[
        'films' => $films
      ]);
    }

}
