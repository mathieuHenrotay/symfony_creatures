<?php
/*
  ./src/Controller/CreatureController.php
*/

namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Creature;
use Symfony\Component\HttpFoundation\Request;
use App\Form\CreatureType;

/**
 * Le controller des créatures
 */
class CreatureController extends GenericController {

    /**
     * renvoie la liste de Creature
     * @param  string  $vue       vue renvoyée
     * @param  array   $orderBy   ordination ['champ'=>'sens']
     * @param  int  $limit     limitation de nombre d'élément
     * @param  boolean $adminPage Définit si on est sur la page qui permet d'administrer les créatures (ajout, suppresion, modification)
     * @return  Symfony\Component\HttpFoundation\Response            Renvoie une vue avec les Creatures disponibles et l'adminPage disponibles
     * @access public
     */
    public function indexAction(string $vue = 'index', array $orderBy = ['dateCreation' => 'DESC'], int $limit = null, bool $adminPage = false){
      $creatures = $this->_repository->findBy([], $orderBy, $limit);
      return $this->render('creatures/'.$vue.'.html.twig',[
        'creatures' => $creatures,
        'adminPage' => $adminPage
      ]);
    }

    /**
     * Permet de modifier une Crature existante
     * @param  Creature $creature Creature à modfier
     * @param  Request  $request  Reponse lors de l'envoi du form
     * @return Symfony\Component\HttpFoundation\RedirectResponse|Symfony\Component\HttpFoundation\Response             Renvoie une vue permet la modification (1ère exec) puis redirige vers la route permettant l'affichage de la créature modifiée (2ème exec)
     * @access public
     */
    public function editAction(Creature $creature, Request $request) {
      $form = $this->createForm(CreatureType::class, $creature);

      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
        $manager = $this->getDoctrine()->getManager();
        $creature->setSlug($creature->slugify($creature->getNom()));
        $manager->flush();

        $this->get('session')->getFlashBag()->clear();
        $this->addFlash('message', "la créature a bien été modifiée");
        return $this->redirectToRoute('app_creatures_show', ['id'=> $creature->getId(), 'slug'=> $creature->getSlug()]);
      }

      return $this->render('creatures/edit.html.twig', [
        	'creature' => $creature,
          'form' => $form->createView()
      ]);
    }

    /**
     * Permet d'ajouter une nouvelle Creature
     * @param Request $request retour du formulaire
     * @return Symfony\Component\HttpFoundation\RedirectResponse|Symfony\Component\HttpFoundation\Response             Renvoie une vue permet la modification (1ere exec) puis redirige vers la route permettant l'affichage de la créature ajoutée (2ème exec)
     * @access public
     */
    public function addAction(Request $request) {
      $creature = new Creature();
      $form = $this->createForm(CreatureType::class, $creature);

      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
        $manager = $this->getDoctrine()->getManager();
        $creature->setSlug($creature->slugify($creature->getNom()));
        $manager->persist($creature);
        $manager->flush();

        $this->get('session')->getFlashBag()->clear();
        $this->addFlash('message', "la créature a bien été créée");
        return $this->redirectToRoute('app_creatures_show', ['id'=> $creature->getId(), 'slug' => $creature->getSlug()]);
      }

      return $this->render('creatures/add.html.twig', [
          'creature' => $creature,
          'form' => $form->createView()
      ]);
    }

    /**
     * Suppression d'une Creature
     * @param  Creature $creature Creature a supprimer
     * @param  Request  $request  récupération du token nécessaire à la suppression
     * @return Symfony\Component\HttpFoundation\RedirectResponse             Redirige vers la page d'adminitration des créatures
     * @access public
     */
    public function deleteAction(Creature $creature, Request $request){
      if ($this->isCsrfTokenValid('delete'.$creature->getId(), $request->get('_token'))){
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($creature);
        $manager->flush();

        $this->get('session')->getFlashBag()->clear();
        $this->addFlash('message', "La créature a bien été supprimée");
      }
      return $this->redirectToRoute('app_pages_show', ['id'=>2, 'slug'=>'creatures']);
    }

    /**
     * Recherche sur les creatures
     * @param  Request $request Récupère la recherche tapée par l'utilisateur
     * @return Symfony\Component\HttpFoundation\Response           affiche la vue des résultats
     * @access public
     */
    public function searchAction(Request $request){
      $search = $request->query->get('search');
      //$results = $this->_repository->findAllBySearch($search);
      $creatures = $this->_repository->findAllBySearch($search);
      //le score ne m'étant pas utile dans la vue je l'exclue de mon tableau final
      //$creatures=[];
      //foreach ($results as $result) {
        //array_push($creatures, $result[0]);
      //}
      return $this->render('creatures/search.html.twig', [
          'creatures' => $creatures,
          'search'    => $search
      ]);
    }

}
