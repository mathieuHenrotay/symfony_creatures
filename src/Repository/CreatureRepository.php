<?php
// ./src/Repository/CreatureRepository.php
namespace App\Repository;

use App\Entity\Creature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * @method Creature|null find($id, $lockMode = null, $lockVersion = null)
 * @method Creature|null findOneBy(array $criteria, array $orderBy = null)
 * @method Creature[]    findAll()
 * @method Creature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * Repository de Creature
 */
class CreatureRepository extends ServiceEntityRepository
{
    /**
     * constructeur automatique
     * @param RegistryInterface $registry Objet nécessaire au constructeur parent
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Creature::class);
    }


    /**
     * Permet de récupérer les Creatures qui correspondent à la recherche
     * @param  string $search mot ou liste de mots rentrés par l'utilisateur
     * @return Creature[]         renvoit un tableau d'objets Creature
     */
     public function findAllBySearch(string $search){
           //realisé en utilisant requête native
           // j'explose la chaine et je l'envoie dans un tableau
           $words = explode(' ', $search);
           //pour chaque élément je rajoute une asterisque à la fin (utilie pour le match against)
           foreach ($words as $key => $value) {
             $words[$key]=$value.'*';
           }
           //je recrée une chaine avec les nouvelles valeurs
           $search = implode(" ", $words);

           $rsm = new ResultSetMappingBuilder($this->getEntityManager());
           $rsm->addRootEntityFromClassMetadata('App\Entity\Creature', 'c');

           $sql = "SELECT c.*
                 FROM creature c
                 LEFT JOIN creature_tag ct ON ct.creature_id = c.id
                 LEFT JOIN tag t ON ct.tag_id = t.id
                 LEFT JOIN film f on c.film_id = f.id
                 WHERE MATCH(c.nom, c.texte_lead, c.texte_Suite) AGAINST(:search IN BOOLEAN MODE)+MATCH(t.nom) AGAINST(:search IN BOOLEAN MODE)+MATCH(f.titre) AGAINST(:search IN BOOLEAN MODE) >0
                 GROUP BY c.id
                 ORDER BY sum(MATCH(c.nom, c.texte_lead, c.texte_Suite) AGAINST(:search IN BOOLEAN MODE)+MATCH(t.nom) AGAINST(:search IN BOOLEAN MODE)+MATCH(f.titre) AGAINST(:search IN BOOLEAN MODE)) DESC, c.date_creation DESC";

           $query = $this->_em->createNativeQuery($sql, $rsm);
           $query->setParameter('search', $search);
           return $query->getResult();
     }


/*     public function findAllBySearch(string $search){
      $words = explode(' ', $search);
      $qb = $this->createQueryBuilder('c');
      $qb->leftJoin('c.film', 'f');
      $qb->leftJoin('c.tags', 't');
      $qb->andWhere('1=0');
      foreach ($words as $key => $word) {
        $qb->orWhere($qb->expr()->orX(
                    $qb->expr()->like('c.nom', ':val'.$key),
                    $qb->expr()->like('c.texteLead', ':val'.$key),
                    $qb->expr()->like('c.texteSuite', ':val'.$key),
                    $qb->expr()->like('f.titre', ':val'.$key),
                    $qb->expr()->like('t.nom', ':val'.$key)
                  ))
        ->setParameter('val'.$key, '%'.$word.'%');
      }
      return $qb->orderBy('c.dateCreation', 'DESC')
                //->setMaxResults(10) //utile pour limiter les ressources utilisées
                ->getQuery()
                ->getResult();
     }*/


/*        //en utilisant le query builder
          // j'explose la chaine et je l'envoie dans un tableau
          $words = explode(' ', $search);
          //pour chaque élément je rajoute une asterisque à la fin (utilie pour le match against)
          foreach ($words as $key => $value) {
            $words[$key]=$value.'*';
          }
          //je recrée une chaine avec les nouvelles valeurs
          $search = implode(" ", $words);
          $qb = $this->createQueryBuilder('c');
          //le addSelect est obligatoire car sans lui je ne peut pas faire l'orderBy (j'ai une erreur sur le + quand je note le match against dans le orderBy)
          $qb->addselect("MATCH_AGAINST(c.nom, c.texteLead, c.texteSuite, :searchterm 'IN BOOLEAN MODE')+MATCH_AGAINST(t.nom, :searchterm 'IN BOOLEAN MODE')+MATCH_AGAINST(f.titre, :searchterm 'IN BOOLEAN MODE') as score");
          $qb->leftJoin('c.tags', 't');
          $qb->leftJoin('c.film', 'f');
          $qb->andWhere("MATCH_AGAINST(c.nom, c.texteLead, c.texteSuite, :searchterm 'IN BOOLEAN MODE')+MATCH_AGAINST(t.nom, :searchterm 'IN BOOLEAN MODE')+MATCH_AGAINST(f.titre, :searchterm 'IN BOOLEAN MODE') > 0");
          //$qb->andWhere("MATCH_AGAINST(c.nom, c.texteLead, c.texteSuite, :searchterm 'IN BOOLEAN MODE') > 0");
          //$qb->orWhere("MATCH_AGAINST(t.nom, :searchterm 'IN BOOLEAN MODE') > 0");
          //$qb->orWhere("MATCH_AGAINST(f.titre, :searchterm 'IN BOOLEAN MODE') > 0");
          $qb->orderBy("score", 'desc');
          $qb->addOrderBy("c.dateCreation", 'desc');
          //$qb->orderBy("MATCH_AGAINST(c.nom, c.texteLead, c.texteSuite, :searchterm 'IN BOOLEAN MODE')+MATCH_AGAINST(t.nom, :searchterm 'IN BOOLEAN MODE')+MATCH_AGAINST(f.titre, :searchterm 'IN BOOLEAN MODE')", 'desc');

          $qb->setParameter('searchterm', $search);
          return $qb->getQuery()->getResult();
    }*/



/*    public function findAllBySearch(string $search){
          $words = [];
          $words = explode(' ', $search);
          $qb = $this->createQueryBuilder('c');
          $qb->leftJoin('c.film', 'f');
          $qb->leftJoin('c.tags', 't');
          $qb->andWhere($qb->expr()->orX(
                        $qb->expr()->like('c.nom', ':val'),
                        $qb->expr()->like('c.texteLead', ':val'),
                        $qb->expr()->like('c.texteSuite', ':val'),
                        $qb->expr()->like('f.titre', ':val'),
                        $qb->expr()->like('t.nom', ':val')
                      ))
              ->setParameter('val', '%'.array_values($words).'%');
          return $qb->orderBy('c.dateCreation', 'DESC')
                    ->setMaxResults(10) //utile pour limiter les ressources utilisées
                    ->getQuery()
                    ->getResult()
      ;
    }*/

    // /**
    //  * @return Creature[] Returns an array of Creature objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Creature
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
